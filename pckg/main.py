import sys
from hug import print_hello


def main():
    print("Enter your name:")
    name = sys.stdin.readline().strip()
    print_hello(name)


if __name__ == "__main__":
    main()