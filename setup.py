from setuptools import setup, find_namespace_packages


setup(
    name="cute_hello",
    version="1.0",
    author="ogarokpeter",
    author_email="ogarokpeter@gmail.com",
    # include_package_data=True,
    license="GNU LGPL",
    install_requires=['tqdm'],
    entry_points={'console_scripts' : ['cute_hello = main.main:main']},
    # package_data={'': ['main.py', 'hug.py', '__init__.py']},
    # packages=find_namespace_packages(include=['pckg']),
    platforms=['Linux'],
    keywords=['cuteness'],
    description="Hello world, but in a cute way.",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: People I Like',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.7.3'
        ]
    )